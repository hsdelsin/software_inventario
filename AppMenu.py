# Clase que invoca el menú principal
import sys
from PyQt5 import uic, QtWidgets
from main import VentanaPrincipal
from PyQt5.QtCore import Qt
archivo="frmmenu.ui"
ventana,QtBaseClass=uic.loadUiType(archivo)
class AppMenu(QtWidgets.QMainWindow,ventana):
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        ventana.__init__(self)
        self.setupUi(self)
        self.setWindowFlags(
            Qt.Window |
            Qt.CustomizeWindowHint |
            Qt.WindowTitleHint |
            Qt.WindowCloseButtonHint             
            )
        
        self.btsalir.clicked.connect(self.retornar)
        self.btsimple.clicked.connect(self.ventana1)
    def ventana1(self):
        self.window=QtWidgets.QMainWindow()
        self.window=VentanaPrincipal()
        self.window.show()
        
    def retornar(self):
        self.close()
        
if __name__=="__main__":
    ap=QtWidgets.QApplication(sys.argv)
    ob=AppMenu()
    ob.show()
    sys.exit(ap.exec_())