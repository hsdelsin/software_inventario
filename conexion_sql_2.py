import sqlite3

class Comunicacion2():
    def __init__(self):
        self.conexion = sqlite3.connect('base_datos.db')
        
    def inserta_producto2(self,codigo, nombre, tamaño, tipo, material, costo, resistencia):
        cursor = self.conexion.cursor()
        bd = '''INSERT INTO tabla_datos2 (CODIGO, NOMBRE, TAMAÑO, TIPO, MATERIAL, COSTO, RESISTENCIA)
        VALUES('{}','{}','{}','{}','{}','{}', '{}')'''.format(codigo, nombre, tamaño, tipo, material, costo, resistencia)
        cursor.execute(bd)
        self.conexion.commit()
        cursor.close()
        
    def mostrar_productos2(self):
        cursor = self.conexion.cursor()
        bd = "SELECT * FROM tabla_datos2 "
        cursor.execute(bd)
        registro = cursor.fetchall()
        return registro
    
    def busca_producto2(self, nombre_producto):
        cursor = self.conexion.cursor()
        bd = '''SELECT * FROM tabla_datos2 WHERE NOMBRE = {}'''.format(nombre_producto)
        cursor.execute(bd)
        nombreX= cursor.fetchall()
        cursor.close()
        return nombreX
    
    def elimina_productos2(self, nombre):
        cursor = self.conexion.cursor()
        bd = '''DELETE FROM tabla_datos2 WHERE NOMBRE = {}'''.format(nombre)
        cursor.execute(bd)
        self.conexion.commit()
        cursor.close()
        
    def actualiza_productos2(self, Id, codigo, nombre, tamaño, tipo, material, costo, resistencia):
        cursor = self.conexion.cursor()
        bd= '''UPDATE tabla_datos2 SET CODIGO = '{}', NOMBRE = '{}', TAMAÑO = '{}', TIPO = '{}', MATERIAL = '{}', COSTO = '{}', RESISTENCIA = '{}'
        WHERE ID = '{}' '''.format(codigo, nombre, tamaño, tipo, material, costo, resistencia, Id)
        cursor.execute(bd)
        a = cursor.rowcount
        self.conexion.commit()
        cursor.close()
        return a
        