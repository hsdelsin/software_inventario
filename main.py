import sys
from PyQt5.QtWidgets import QApplication, QMainWindow
from PyQt5 import QtCore, QtWidgets, uic
from conexion_sql import Comunicacion
from conexion_sql_2 import Comunicacion2
archivo="diseño.ui"
ventana, QtBaseclass=uic.loadUiType(archivo)
class VentanaPrincipal(QMainWindow,ventana):
    def __init__(self):
        super(VentanaPrincipal, self).__init__()
        self.setupUi(self)   
        self.setFixedSize(771, 569)
        self.base_datos = Comunicacion()
        self.base_datos2 = Comunicacion2()
        
        self.bt_recargar.clicked.connect(self.mostrar_productos)
        self.bt_recargar_2.clicked.connect(self.mostrar_productos2)
        self.bt_agregar.clicked.connect(self.registrar_productos)
        self.bt_agregar_2.clicked.connect(self.registrar_productos2)
        self.bt_borrar.clicked.connect(self.eliminar_productos)
        self.bt_borrar_2.clicked.connect(self.eliminar_productos2)
        self.bt_actualizar_tabla.clicked.connect(self.modificar_productos)
        self.bt_actualizar_buscar.clicked.connect(self.buscar_por_nombre_actualiza)
        self.bt_actualizar_tabla_2.clicked.connect(self.modificar_productos2)
        self.bt_actualizar_buscar_2.clicked.connect(self.buscar_por_nombre_actualiza2)
        
        self.bt_buscar_borrar.clicked.connect(self.buscar_por_nombre_eliminar)
        self.bt_buscar_borrar_2.clicked.connect(self.buscar_por_nombre_eliminar2)
        
        self.bt_salir.clicked.connect(lambda:self.close())
           

        self.bt_datos.clicked.connect(lambda: self.stackedWidget.setCurrentWidget(self.page_datos))
        self.bt_datos_2.clicked.connect(lambda: self.stackedWidget.setCurrentWidget(self.page_datos2))
        self.bt_registrar.clicked.connect(lambda: self.stackedWidget.setCurrentWidget(self.page_registrar))
        self.bt_registrar_2.clicked.connect(lambda: self.stackedWidget.setCurrentWidget(self.page_registrar2))
        self.bt_actualizar.clicked.connect(lambda: self.stackedWidget.setCurrentWidget(self.page_actualizar))
        self.bt_actualizar_2.clicked.connect(lambda: self.stackedWidget.setCurrentWidget(self.page_actualizar2))
        self.bt_eliminar.clicked.connect(lambda: self.stackedWidget.setCurrentWidget(self.page_eliminar))
        self.bt_eliminar_2.clicked.connect(lambda: self.stackedWidget.setCurrentWidget(self.page_eliminar2))
        
        self.bt_envases.clicked.connect(lambda: self.stackedWidget_2.setCurrentWidget(self.page_envase))
        self.bt_envases.clicked.connect(lambda: self.stackedWidget.setCurrentWidget(self.page_datos2))
        self.bt_insumos.clicked.connect(lambda: self.stackedWidget_2.setCurrentWidget(self.page_insumo))
        self.bt_insumos.clicked.connect(lambda: self.stackedWidget.setCurrentWidget(self.page_datos))

    def mostrar_productos(self):
        datos = self.base_datos.mostrar_productos()
        i = len(datos)
        self.tabla_productos.setRowCount(i)
        tablerow = 0
        for row in datos:
            self.Id = row[0]
            self.tabla_productos.setItem(tablerow,0,QtWidgets.QTableWidgetItem(row[1]))
            self.tabla_productos.setItem(tablerow,1,QtWidgets.QTableWidgetItem(row[2]))
            self.tabla_productos.setItem(tablerow,2,QtWidgets.QTableWidgetItem(row[3]))
            self.tabla_productos.setItem(tablerow,3,QtWidgets.QTableWidgetItem(row[4]))
            self.tabla_productos.setItem(tablerow,4,QtWidgets.QTableWidgetItem(row[5]))
            self.tabla_productos.setItem(tablerow,5,QtWidgets.QTableWidgetItem(row[6]))
            tablerow+=1
            self.signal_actualizar.setText("")
            self.signal_registrar.setText("")
            self.signal_eliminacion.setText("")
            
    def mostrar_productos2(self):
        datos = self.base_datos2.mostrar_productos2()
        i = len(datos)
        self.tabla_productos_2.setRowCount(i)
        tablerow = 0
        for row in datos:
            self.Id = row[0]
            self.tabla_productos_2.setItem(tablerow,0,QtWidgets.QTableWidgetItem(row[1]))
            self.tabla_productos_2.setItem(tablerow,1,QtWidgets.QTableWidgetItem(row[2]))
            self.tabla_productos_2.setItem(tablerow,2,QtWidgets.QTableWidgetItem(row[3]))
            self.tabla_productos_2.setItem(tablerow,3,QtWidgets.QTableWidgetItem(row[4]))
            self.tabla_productos_2.setItem(tablerow,4,QtWidgets.QTableWidgetItem(row[5]))
            self.tabla_productos_2.setItem(tablerow,5,QtWidgets.QTableWidgetItem(row[6]))
            self.tabla_productos_2.setItem(tablerow,6,QtWidgets.QTableWidgetItem(row[7]))
   
            tablerow+=1
            self.signal_actualizar_2.setText("")
            self.signal_registrar_2.setText("")
            self.signal_eliminacion_2.setText("")
            
        
    def registrar_productos(self):
        codigo = self.reg_codigo.text().upper()
        nombre = self.reg_nombre.text().upper()
        tipo = self.reg_modelo.text().upper()
        calidad = self.reg_calidad.text().upper()
        precio = self.reg_precio.text().upper()
        cantidad = self.reg_cantidad.text().upper()
        if codigo != '' and nombre !='' and tipo != '' and calidad != '' and precio != '' and cantidad != '':
            self.base_datos.inserta_producto(codigo, nombre, tipo, calidad, precio, cantidad)
            self.signal_registrar.setText('Productos Registrados')
            self.reg_codigo.clear()
            self.reg_nombre.clear()
            self.reg_modelo.clear()
            self.reg_calidad.clear()
            self.reg_precio.clear()
            self.reg_cantidad.clear()
        else:
            self.signal_registrar.setText('Hay Espacios Vacios')
    
    def registrar_productos2(self):
        codigo = self.reg_codigo_2.text().upper()
        nombre = self.reg_nombre_2.text().upper()
        tamaño = self.reg_tamano.text().upper()
        tipo = self.reg_modelo_2.text().upper()
        material = self.reg_material.text().upper()
        costo = self.reg_costo.text().upper()
        resistencia = self.reg_resistencia.text().upper()
        if codigo != '' and nombre !='' and tamaño != '' and tipo != '' and material != '' and costo != '' and resistencia != '':
            self.base_datos2.inserta_producto2(codigo, nombre, tamaño, tipo, material, costo, resistencia)
            self.signal_registrar_2.setText('Productos Registrados')
            self.reg_codigo_2.clear()
            self.reg_nombre_2.clear()
            self.reg_tamano.clear()
            self.reg_modelo_2.clear()
            self.reg_material.clear()
            self.reg_costo.clear()
            self.reg_resistencia.clear()
        else:
            self.signal_registrar_2.setText('Hay Espacios Vacios')
    
    def buscar_por_nombre_actualiza(self):
        id_producto = self.act_buscar.text().upper()
        id_producto = str("'" + id_producto + "'")
        self.producto = self.base_datos.busca_producto(id_producto)
        if len(self.producto) !=0:
            self.Id = self.producto[0][0]
            self.act_codigo.setText(self.producto[0][1])
            self.act_nombre.setText(self.producto[0][2])
            self.act_modelo.setText(self.producto[0][3])
            self.act_calidad.setText(self.producto[0][4])
            self.act_precio.setText(self.producto[0][5])
            self.act_cantidad.setText(self.producto[0][6])
        else:
            self.signal_actualizar.setText("NO EXISTE")
            
    def buscar_por_nombre_actualiza2(self):
        id_producto = self.act_buscar_2.text().upper()
        id_producto = str("'" + id_producto + "'")
        self.producto = self.base_datos2.busca_producto2(id_producto)
        if len(self.producto) !=0:
            self.Id = self.producto[0][0]
            self.act_codigo_2.setText(self.producto[0][1])
            self.act_nombre_2.setText(self.producto[0][2])
            self.act_tamano.setText(self.producto[0][3])
            self.act_modelo_2.setText(self.producto[0][4])
            self.act_material.setText(self.producto[0][5])
            self.act_costo.setText(self.producto[0][6])
            self.act_resistencia.setText(self.producto[0][7])
        else:
            self.signal_actualizar_2.setText("NO EXISTE")
            
    def modificar_productos(self):
        if self.producto !='':
            codigo = self.act_codigo.text().upper()
            nombre = self.act_nombre.text().upper()
            modelo = self.act_modelo.text().upper()
            calidad = self.act_calidad.text().upper()
            precio = self.act_precio.text().upper()
            cantidad = self.act_cantidad.text().upper()
            act = self.base_datos.actualiza_productos(self.Id, codigo, nombre, modelo, calidad, precio, cantidad)
            if act == 1:
                self.signal_actualizar.setText("ACTUALIZADO")
                self.act_codigo.clear()
                self.act_nombre.clear()
                self.act_modelo.clear()
                self.act_calidad.clear()
                self.act_precio.clear()
                self.act_cantidad.clear()
                self.act_buscar.setText('')
            elif act == 0:
                self.signal_actualizar.setText("ERROR")
            else:
                self.signal_actualizar.setText("INCORRECTO")

            
    def modificar_productos2(self):
        if self.producto !='':
            codigo = self.act_codigo_2.text().upper()
            nombre = self.act_nombre_2.text().upper()
            tamaño = self.act_tamano.text().upper()
            tipo = self.act_modelo_2.text().upper()
            material = self.act_material.text().upper()
            costo = self.act_costo.text().upper()
            resistencia = self.act_resistencia.text().upper()
            act = self.base_datos2.actualiza_productos2(self.Id, codigo, nombre, tamaño, tipo, material, costo, resistencia)
            if act == 1:
                self.signal_actualizar_2.setText("ACTUALIZADO")
                self.act_codigo_2.clear()
                self.act_nombre_2.clear()
                self.act_tamano.clear()
                self.act_modelo_2.clear()
                self.act_material.clear()
                self.act_costo.clear()
                self.act_resistencia.clear()
                self.act_buscar_2.setText('')
            elif act == 0:
                self.signal_actualizar_2.setText("ERROR")
            else:
                self.signal_actualizar_2.setText("INCORRECTO")
    
    def buscar_por_nombre_eliminar(self):
        nombre_producto = self.eliminar_buscar.text().upper()
        nombre_producto = str("'" + nombre_producto + "'")
        producto = self.base_datos.busca_producto(nombre_producto)
        self.tabla_borrar.setRowCount(len(producto))
        
        if len (producto) ==0:
            self.signal_eliminacion.setText('No Existe')
        else:
            self.signal_eliminacion.setText('Producto Seleccionado')
        tablerow = 0
        for row in producto:
            self.producto_a_borrar = row[2]
            self.tabla_borrar.setItem(tablerow,0,QtWidgets.QTableWidgetItem(row[1]))
            self.tabla_borrar.setItem(tablerow,1,QtWidgets.QTableWidgetItem(row[2]))
            self.tabla_borrar.setItem(tablerow,2,QtWidgets.QTableWidgetItem(row[3]))
            self.tabla_borrar.setItem(tablerow,3,QtWidgets.QTableWidgetItem(row[4]))
            self.tabla_borrar.setItem(tablerow,4,QtWidgets.QTableWidgetItem(row[5]))
            self.tabla_borrar.setItem(tablerow,5,QtWidgets.QTableWidgetItem(row[6]))
            tablerow+=1
   
    def buscar_por_nombre_eliminar2(self):
        nombre_producto = self.eliminar_buscar_2.text().upper()
        nombre_producto = str("'" + nombre_producto + "'")
        producto = self.base_datos2.busca_producto2(nombre_producto)
        self.tabla_borrar_2.setRowCount(len(producto))
        
        if len (producto) ==0:
            self.signal_eliminacion_2.setText('No Existe')
        else:
            self.signal_eliminacion_2.setText('Producto Seleccionado')
        tablerow = 0
        for row in producto:
            self.producto_a_borrar = row[2]
            self.tabla_borrar_2.setItem(tablerow,0,QtWidgets.QTableWidgetItem(row[1]))
            self.tabla_borrar_2.setItem(tablerow,1,QtWidgets.QTableWidgetItem(row[2]))
            self.tabla_borrar_2.setItem(tablerow,2,QtWidgets.QTableWidgetItem(row[3]))
            self.tabla_borrar_2.setItem(tablerow,3,QtWidgets.QTableWidgetItem(row[4]))
            self.tabla_borrar_2.setItem(tablerow,4,QtWidgets.QTableWidgetItem(row[5]))
            self.tabla_borrar_2.setItem(tablerow,5,QtWidgets.QTableWidgetItem(row[6]))
            self.tabla_borrar_2.setItem(tablerow,5,QtWidgets.QTableWidgetItem(row[7]))
            tablerow+=1
    
    def eliminar_productos(self):
        self.row_flag=self.tabla_borrar.currentRow()
        if self.row_flag ==0:
            self.tabla_borrar.removeRow(0)
            self.base_datos.elimina_productos("'" + self.producto_a_borrar + "'")
            self.signal_eliminacion.setText('Producto Eliminado')
            self.eliminar_buscar.setText('')
            
    def eliminar_productos2(self):
        self.row_flag=self.tabla_borrar_2.currentRow()
        if self.row_flag ==0:
            self.tabla_borrar_2.removeRow(0)
            self.base_datos2.elimina_productos2("'" + self.producto_a_borrar + "'")
            self.signal_eliminacion_2.setText('Producto Eliminado')
            self.eliminar_buscar_2.setText('')



if __name__=="__main__":
    app = QApplication(sys.argv)
    mi_app = VentanaPrincipal()
    mi_app.show()
    sys.exit(app.exec_())

            
        
        
        


